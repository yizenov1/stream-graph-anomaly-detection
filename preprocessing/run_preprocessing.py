from preprocessing import basic_preprocess


def run_module(tsv_file, column_number):

    print("\nReport of initial raw data")

    empty_lines_number = basic_preprocess.read_empty_lines(tsv_file)
    print("\tNumber of missing lines: " + str(empty_lines_number))

    tsv_file.seek(0)
    empty_values_number = basic_preprocess.read_empty_values(tsv_file, column_number)
    print("\tNumber of missing values: " + str(empty_values_number))

    tsv_file.seek(0)
    return tsv_file


def create_report():
    print()
