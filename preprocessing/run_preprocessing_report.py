import os

from loading_input import read_input
from preprocessing import basic_preprocess

cur_path = os.path.dirname(__file__)
tsv_path = "../inputs/youtube.tsv"  # TODO: this parameter should be defined
abs_tsv_path = os.path.relpath(tsv_path, cur_path)

column_number = 6  # TODO: this parameter should be defined
print "\nReport of initial raw data"

tsv_path = read_input.read_tsv_file(abs_tsv_path)
empty_lines_number = basic_preprocess.read_empty_lines(tsv_path)
print "\tNumber of missing lines: " + str(empty_lines_number)

tsv_path.seek(0)
empty_values_number = basic_preprocess.read_empty_values(tsv_path, column_number)
print "\tNumber of missing values: " + str(empty_values_number)


