

def read_empty_lines(file_object):
    counter = 0
    for line in file_object:
        line = line.strip()
        if not line:
            counter += 1
    return counter


def read_empty_values(file_object, column_number):
    counter = 0
    for line in file_object:
        line = line.strip()
        if line:
            values = line.split()
            if len(values) is not column_number:
                    counter += column_number - len(values)
    return counter
