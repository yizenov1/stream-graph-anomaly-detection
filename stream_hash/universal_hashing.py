import uuid


def run_hashing(shingle_vector_tuples, s_max, random_numbers):

    hashed_binary_matrices = []
    for key in shingle_vector_tuples:
        hashed_binary_matrix = run_hashing_per_shingle(key, s_max, random_numbers)
        hashed_binary_matrices.append(hashed_binary_matrix)
    return hashed_binary_matrices


def run_hashing_per_shingle(shingle, s_max, random_numbers):

    length = len(shingle)
    if length < s_max:
        i = 0
        while i < s_max - length:
            shingle += ' '
            i += 1
    elif length > s_max:
        shingle = shingle[0:s_max]

    hash_values = []
    for random_number in random_numbers:
        hash_values.append(hashing_function(shingle, random_number))
    return hash_values


def generate_randoms_for_universal_hashing(s_max, number_of_hash_functions):

    i = 0
    hash_functions_random_numbers = []
    while i < number_of_hash_functions:
        j = 0
        rand_numbers = []
        while j < s_max:
            rand_numbers.append(uuid.uuid1().int >> 64)
            j += 1
        hash_functions_random_numbers.append(rand_numbers)
        i += 1
    return hash_functions_random_numbers


def hashing_function(input_param, random_numbers):
    chars = list(input_param)
    hash_value = random_numbers[0]

    i = 1
    while i < len(random_numbers):
        hash_value += random_numbers[i] * ord(chars[i])
        i += 1

    # (hash_value % 2) is either '0' or '1'
    hash_value = 2 * (hash_value % 2) - 1  # output is either '-1' or '+1'

    return hash_value
