from stream_hash import universal_hashing
from stream_hash import static_sim_hash


# Universal hashing definition:
# 1. +1 and -1 equally probable over all hash functions
# 2. non-trivial uniform families, equally probable over the universe
# 3. pairwise-independent


# Difference between streaming and batch hashing:
# 1. no longer need to know complete shingle universe
# 2. or maintain random vectors in memory
# 3. streaming uses random hash functions instead of random vectors


def run_module(graphs, max_shingle_length, number_of_hash_functions):

    # creating shingle vector and creating shingle-frequency vector for each graph
    frequency_vectors = []
    for graph in graphs.items():
        shingle_frequency_vector = static_sim_hash.create_shingles_vector(graph)
        frequency_vectors.append(shingle_frequency_vector)

    global_random_numbers = generate_hash_functions(max_shingle_length, number_of_hash_functions)

    hashed_binary_matrices = []
    for frequency_vector in frequency_vectors:
        hashed_binary_matrices.append(compute_hashing(frequency_vector.keys(), global_random_numbers, max_shingle_length))

    projection_vectors = []
    i = 0
    for hashed_binary_matrix in hashed_binary_matrices:
        projection_vectors.append(compute_projections(hashed_binary_matrix, list(frequency_vectors[i].values())))
        i += 1

    sketch_vectors = []
    for projection_vector in projection_vectors:
        sketch_vectors.append(compute_sketch_vectors(projection_vector))

    return sketch_vectors


def generate_hash_functions(s_max, number_of_hash_functions):
    return universal_hashing.generate_randoms_for_universal_hashing(s_max, number_of_hash_functions)


def compute_hashing(frequency_vector, global_random_numbers, s_max):
    return universal_hashing.run_hashing(frequency_vector, s_max, global_random_numbers)


def compute_projections(hashed_binary_matrix, frequency_vector_occurrences):
    projection_vector = []
    number_of_hash_functions = len(hashed_binary_matrix[0])
    number_of_shingles = len(frequency_vector_occurrences)
    i = 0

    while i < number_of_hash_functions:
        dot_product = 0.0
        j = 0
        while j < number_of_shingles:
            dot_product += frequency_vector_occurrences[j] * hashed_binary_matrix[j][i]
            j += 1
        projection_vector.append(dot_product)
        i += 1

    return projection_vector


def compute_sketch_vectors(projection_vector):
    sketch_vector = []
    for value in projection_vector:
        if value >= 0:
            sketch_vector.append(1)
        else:
            sketch_vector.append(-1)
    return sketch_vector
