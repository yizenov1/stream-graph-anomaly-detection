import numpy as np


def create_gaussian_vectors(number_of_vectors, vector_size):

    random_vectors = []
    mean, sigma = 0, 0.1  # TODO: mean and standard deviation

    i = 0
    while i < number_of_vectors:
        random_vector = np.random.normal(mean, sigma, vector_size)
        random_vectors.append(random_vector)
        i += 1

    return random_vectors


def create_uniform_vectors(number_of_vectors, vector_size):
    random_vectors = []
    return random_vectors
