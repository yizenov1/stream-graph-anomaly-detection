import networkx as nx
import numpy as np
from stream_hash import create_random_vectors as crv


# TODO: 1: size of shingle universe can get too big, 2: have to know the size of shingle universe
# TODO: 3: need to keep projection vectors(including randoms) in memory


# TODO: delete unneeded graphs and shingle vectors from the memory
# TODO: replace node, edge and other information with bits to save memory
def run_static_sim_hash(graphs):
    universe_shingles = set("Universe shingles")
    frequency_vectors = []
    for graph_id, graph in graphs.items():
        shingle_frequency_vector = create_shingles_vector(graph)
        create_universe_shingles(universe_shingles, shingle_frequency_vector)
        frequency_vectors.append(shingle_frequency_vector)

    same_size_frequency_vectors = []
    for frequency_vector in frequency_vectors:
        same_size_vector = create_same_size_vectors(frequency_vector, universe_shingles)
        same_size_frequency_vectors.append(same_size_vector)

    number_of_vectors = 10  # TODO: need to choose a proper number of vectors
    random_vectors = crv.create_gaussian_vectors(number_of_vectors, len(universe_shingles))
    hashing_vectors = create_vector_hashings(same_size_frequency_vectors, random_vectors)

    for hashing_vector in hashing_vectors:
        print(hashing_vector)


# create shingles vector per graph
# for each node perform BFS(1-hope) and create shingle for each node
# create distinct shingle vector and their occurrence
def create_shingles_vector(graph):

    distinct_shingles_with_occurrence = {}

    for node_id, node_attribute in graph.nodes_iter(data=True):
        bfs = nx.bfs_edges(graph, node_id)
        shingle = node_attribute['node_type']
        for edge in bfs:
            if edge[0] == node_id:
                edge_type = graph[edge[0]][edge[1]]['weight']
                # shingle += " " + edge_type + " " + edge[1]
                shingle += edge_type + graph.node[edge[1]]['node_type']

        if shingle in distinct_shingles_with_occurrence:
            distinct_shingles_with_occurrence[shingle] += 1.0
        else:
            distinct_shingles_with_occurrence.setdefault(shingle, 1.0)

    return distinct_shingles_with_occurrence


# TODO: have to know the universe of shingles
def create_universe_shingles(universe_shingles, graph_shingle_frequency_vector):
    for shingle in graph_shingle_frequency_vector:
        if shingle not in universe_shingles:
            universe_shingles.add(shingle)


def create_same_size_vectors(frequency_vector, universe_shingles):
    new_vector = {}
    for universe_shingle in universe_shingles:
        if universe_shingle in frequency_vector:
            new_vector.setdefault(universe_shingle, frequency_vector[universe_shingle])
        else:
            new_vector.setdefault(universe_shingle, 0.0)

    return new_vector


def create_vector_hashings(same_size_frequency_vectors, random_vectors):
    new_hashing_vectors = []
    for same_size_frequency_vector in same_size_frequency_vectors:
        new_hashing_vector = []

        arrays = np.array(list(same_size_frequency_vector.values()), dtype=float)
        for random_vector in random_vectors:
            dot_product = np.dot(random_vector, arrays)
            if dot_product >= 0.0:
                new_hashing_vector.append(1.0)
            else:
                new_hashing_vector.append(-1.0)
        new_hashing_vectors.append(new_hashing_vector)
    return new_hashing_vectors