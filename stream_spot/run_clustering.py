import numpy as np
import pyclust


def run_clustering(training_data, number_of_clusters, number_of_trials):
    sequence_vertically_arranged = np.vstack(training_data)
    index_arr = np.arange(sequence_vertically_arranged.shape[0])
    np.random.shuffle(index_arr)
    sequence_vertically_arranged = sequence_vertically_arranged[index_arr, :]  # rearranges according to index array
    k_medoids_cluster = pyclust.KMedoids(n_clusters=number_of_clusters, n_trials=number_of_trials)
    k_medoids_cluster.fit(sequence_vertically_arranged)

    print()
    print(k_medoids_cluster)

    return k_medoids_cluster


def merge_sketches():
    return 0.0