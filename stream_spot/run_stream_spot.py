import random

from stream_spot import run_bootsrap_installation as rbi


# TODO: number of graphs in each cluster
# TODO: <graph, score, assigned cluster>, number of clusters - K + 'attack'
def run_module(sketch_vectors, number_of_clusters, size_of_training_set, number_of_trials):
    training_data = random.sample(sketch_vectors, size_of_training_set)
    k_medoids_cluster = rbi.run_bootstrap_clustering(training_data, number_of_clusters, number_of_trials)
    centroids_projection_vectors = construct_centroid_projection_vectors(k_medoids_cluster)
    centroids_sketches = construct_centroid_sketches(centroids_projection_vectors)


def compute_new_graph():
    print()


def compute_changed_graph():
    print()


def compute_anomaly_score():
    return 0.0


def construct_centroid_projection_vectors(k_medoids_cluster):
    return 0.0


def construct_centroid_sketches(centroids_projection_vectors):
    return 0.0
