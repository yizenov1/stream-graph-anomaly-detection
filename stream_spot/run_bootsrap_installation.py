from stream_spot import run_clustering as rc


# TODO: distance is calculated by euclidean distance in pyclust package
def run_bootstrap_clustering(training_data, number_of_clusters, number_of_trials):
    k_medoids_cluster = rc.run_clustering(training_data, number_of_clusters, number_of_trials)
    return k_medoids_cluster
