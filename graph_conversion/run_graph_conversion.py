from graph_conversion import graph_utils


# this function serves as an interface for different graph module implementations
def run_module(tsv_file):

    graphs = graph_utils.run_graph_conversion(tsv_file)

    return graphs
