import networkx as nx


# this is application specific graph model
# the output is a directed graph
def run_graph_conversion(file_object):

    di_graphs = {}  # key-value dictionary data structure

    for line in file_object:

        line = line.strip()  # removes all whitespace at the start and end

        if line:
            values = line.split()  # splits string input into a set of string separated by space

            # GRAPH FORMULATION/SENSING/MODEL
            ############################################################################################################
            source_id = values[0]  # unique process id per process sequence
            source_type = values[1]  # this can be data/address/argument
            destination_id = values[2]  #
            destination_type = values[3]  #

            # types of edges: line and dashed line
            edge_type = values[4]  # sorted by timestamp, format is (timestamp, function_call_name)
            graph_id = values[5]  # graph per software/browser/OS function(i.e. a set of system calls)

            ############################################################################################################

            # it creates a new graph if it doesn't exist in the set of distinct graphs
            if graph_id not in di_graphs:
                graph = nx.DiGraph()
                di_graphs[graph_id] = graph
                add_input(graph, source_id, source_type, destination_id, destination_type, edge_type)
            else:
                graph = di_graphs.get(graph_id)
                add_input(graph, source_id, source_type, destination_id, destination_type, edge_type)

    return di_graphs


# adds new non-existed nodes first
# an edge is created if two nodes are in the same graph
def add_input(graph, source_id, source_type, destination_id, destination_type, edge_type):

    if not graph.has_node(source_id):
        graph.add_node(source_id, node_type=source_type)

    if not graph.has_node(destination_id):
        graph.add_node(destination_id, node_type=destination_type)

    graph.add_edge(source_id, destination_id, weight=edge_type)
