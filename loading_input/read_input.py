import os


def read_txt_file(txt_path):
    cur_path = os.path.dirname(__file__)
    abs_txt_path = os.path.relpath(txt_path, cur_path)
    txt_file = open(abs_txt_path, "r")
    return txt_file


def read_json_file(json_path):
    cur_path = os.path.dirname(__file__)
    abs_json_path = os.path.relpath(json_path, cur_path)
    json_file = open(abs_json_path, "r")
    return json_file


def read_xml_file(xml_path):
    cur_path = os.path.dirname(__file__)
    abs_xml_path = os.path.relpath(xml_path, cur_path)
    xml_file = open(abs_xml_path, "r")
    return xml_file


def read_tsv_file(tsv_path):
    cur_path = os.path.dirname(__file__)
    abs_tsv_path = os.path.relpath(tsv_path, cur_path)
    tsv_file = open(abs_tsv_path, "r")
    return tsv_file
