from loading_input import read_input
from preprocessing import run_preprocessing
from statistics import run_statistics
from graph_conversion import run_graph_conversion
from graph_statistics import run_graph_statistics
from stream_hash import run_stream_hashing
from stream_hash import run_batch_hashing
from stream_spot import run_stream_spot
from stream_spot import run_batch_spot

# section for parameters
NUMBER_OF_COLUMNS = 6
TSV_FILE_PATH = "../inputs/first_graph.tsv"
# TSV_FILE_PATH = "../inputs/youtube.tsv"
NUMBER_OF_HASH_FUNCTIONS = 4
MAX_LENGTH_OF_SHINGLES = 5
PERCENT_OF_ORIGINAL_DATA_SET = 0.3
NUMBER_OF_CLUSTER = 2
NUMBER_OF_TRIALS = 1

# loading input files stage
Tsv_file = tsv_file = read_input.read_tsv_file(TSV_FILE_PATH)

# raw data processing stages
# run_preprocessing.run_module(Tsv_file, NUMBER_OF_COLUMNS)
# run_statistics.run_module(Tsv_file)

# graph data processing stages
Graphs = run_graph_conversion.run_module(Tsv_file)  # TODO: streaming has to be here
# run_graph_statistics.run_module(Graphs)
Tsv_file.close()
del Tsv_file

# streaming and batch hashing stage
# Sketch_vectors = run_batch_hashing.run_batch_module(Graphs, MAX_LENGTH_OF_SHINGLES, NUMBER_OF_HASH_FUNCTIONS)
Sketch_vectors = run_stream_hashing.run_module(Graphs.keys(), MAX_LENGTH_OF_SHINGLES, NUMBER_OF_HASH_FUNCTIONS) # TODO: update has to be here
Graphs.clear()
del Graphs

# streaming and batch clustering stage
# Training_set_size = int(round(len(Sketch_vectors) * PERCENT_OF_ORIGINAL_DATA_SET))
# Batch_system = run_batch_spot.run_module(Sketch_vectors, NUMBER_OF_CLUSTER, Training_set_size, NUMBER_OF_TRIALS)
# Stream_system = run_stream_spot.run_module(Sketch_vectors, NUMBER_OF_CLUSTER, Training_set_size, NUMBER_OF_TRIALS)

# TODO: online data receiving and anomaly detection
