

def number_of_graphs(graphs):
    counter = len(graphs)
    print("\nNumber of graphs: " + str(counter))


def graph_nodes(graphs):
    for graph_id, graph in graphs.items():
        for node_id, node_attribute in graph.nodes_iter(data=True):
            print("\t" + node_id + " " + node_attribute['node_type'])


def graph_edges(graphs):
    for graph_id, graph in graphs.items():
        for source, destination, edge_attribute in graph.edges_iter(data=True):
            print("\t" + source + " " + destination + " " + edge_attribute['weight'])


def each_graph_nodes(graphs):
    for graph_id, graph in graphs.items():
        print("\tGraph #" + graph_id + ": " + str(len(graph.nodes())))
